import * as cookieMaker from "./childScript.js";

const cookieFlavor = "chocolate chip";
console.log(`I'm hungry for ${cookieFlavor} cookies!`)
cookieMaker.bakeCookie(cookieFlavor);
cookieMaker.bakeCookie(cookieFlavor);
setTimeout(() => {
    console.log(`I had ${cookieMaker.cookiesBaked} ${cookieFlavor} cookies!`)
},1500)