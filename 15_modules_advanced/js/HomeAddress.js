import {address} from './Address.js';

const homeAddress = {
    address: address,
    street: 'Nationalestraat',
    houseNr: 5
}

export {homeAddress};
