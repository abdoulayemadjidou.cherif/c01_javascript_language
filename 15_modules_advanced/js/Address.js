const address = {
    recipient: 'Karel de Grote University of Applied Sciences and Arts',
    postalCode : 2000,
    city: 'Antwerpen',
    country: 'Belgium'
}

export {address};
