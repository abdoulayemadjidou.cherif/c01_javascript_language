import {address} from './Address.js';

const poBox = {
  address: address,
  POBoxNumber: 9001
};

export {poBox};