import {homeAddress} from './HomeAddress.js';
import {poBox} from './POBox.js'

const home = homeAddress;
const POBox = poBox;

const outputElement = document.getElementById('output_area');

outputElement.textContent =
    `${home.address.recipient}
${home.street} ${home.houseNr}
${home.address.postalCode} ${home.address.postalCode}
${home.address.country}

${POBox.address.recipient}
${POBox.POBoxNumber}
${POBox.address.postalCode} ${POBox.address.postalCode}
${POBox.address.country}`;
