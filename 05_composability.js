const people = ["Lars", "Jan", "Ann", "Piet", "Tony", "Tom", "Wim", "Dieter", "Antoon", "Charmaine"];

// Obtain:
// * the upper case version of each person
// * ... whose name has a length of 3
// * ... and concatenate them

const upperCase = people.map(name => name.toUpperCase());
const shortOnes = upperCase.filter(name => name.length === 3);
const result1 = shortOnes.reduce((accum, current) => accum + "-" + current);
console.log("Result 1: " + result1);

// A "Fluent Interface" enhances readability and expressiveness:
const result2 = people
    .map(name => name.toUpperCase())
    .filter(name => name.length === 3)
    .reduce((accum, current) => accum + "-" + current);
console.log("Result 2: " + result2);

// Note: you might want to filter _before_ mapping! Why?
