let things = "crows"

function showMessage(number) {console.log(number + " " + things);}

function makeCounter() {
    let count = 2;

    return function () {
        return count++;
    };
}

let counter = makeCounter();

showMessage(counter()); // 2 crows
showMessage(counter()); // 3 crows
showMessage(counter()); // 4 crows
