function nameValidator(name) {
    if (!name) {
        throw new ReferenceError('\'name\' is mandatory!');
    } else if (typeof name !== 'string') {
        throw new TypeError('\'name\' should be string!');
    }
    console.log(`${name} is valid!`);
}


nameValidator(null, '123');
nameValidator(undefined, '123');
nameValidator('', '123');
nameValidator(7, '123');
nameValidator('Mary', '123');

//More example code

try {
    // In JavaScript, the recommended exception type is 'Error'.
    // https://developer.mozilla.org/en-US/docs/web/javascript/reference/global_objects/error

    // Don't use anything other than Error or its subclasses!
    throw 'apple'; // Don't do this!
} catch (fruit) {
    console.log(`Thanks for the ${fruit}.`);
}