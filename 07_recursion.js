function power  (base, exponent=1)  {
    return (exponent == 0)?1:power(base, exponent - 1) * base;
}

console.log(power(5,4)); //625
console.log(power(6));
console.log(power(6,2));

