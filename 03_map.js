const myStrings = ["Lars", "Jan", "Ann", "Piet", "Tony", "Tom", "Wim", "Dieter", "Antoon", "Charmaine"];

const onlyFirstLetters = myStrings.map(str => str.charAt(0));

console.log("First letters:");
console.log(onlyFirstLetters);

const firstLettersInLowerCase = onlyFirstLetters.map(str => str.toLowerCase());
console.log("... and in lower case:");
console.log(firstLettersInLowerCase);
